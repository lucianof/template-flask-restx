"""empty message

Revision ID: 5998d5b6fcea
Revises: 
Create Date: 2022-03-25 21:01:27.673270

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5998d5b6fcea'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('user',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('username', sa.String(length=512), nullable=False),
    sa.Column('created_at', sa.DateTime(timezone=True), server_default=sa.text('(CURRENT_TIMESTAMP)'), nullable=True),
    sa.Column('last_update', sa.DateTime(timezone=True), nullable=True),
    sa.Column('password_hash', sa.String(length=512), nullable=False),
    sa.Column('salt', sa.String(length=256), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('username')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('user')
    # ### end Alembic commands ###
