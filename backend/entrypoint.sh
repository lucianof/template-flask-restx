#!/bin/bash

flask db upgrade
exec "$@"
gunicorn -c "guinicorn.conf.py" "src:create_app()"
