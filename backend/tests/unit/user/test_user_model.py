from tests.unit.unit_test import UnitTest
from app.src.user.model import user_model


class TestUserModel(UnitTest):
    def setUp(self) -> None:
        super().setUp()
        self.VALID_DATA = {
            'username': 'johnson',
            'password': 'thisisastrongpassword'
        }
        self.user = user_model.UserModel(**self.VALID_DATA)
        self.user.save()

    def test_user_save_into_db(self):
        new_user = user_model.UserModel(username='random', password='anotherpass')
        new_user.save()
        self.assertEqual(new_user.id, 2)

    def test_user_password_cannot_be_accessed(self):
        self.assertRaises(AttributeError, getattr, self.user, 'password')

    def test_user_password_valid(self):
        self.assertTrue(self.user.check_password(self.VALID_DATA['password']))

    def test_user_password_invalid(self):
        self.assertFalse(self.user.check_password('wrongpassword'))
