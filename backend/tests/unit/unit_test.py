from unittest import TestCase

from app import create_app
from app.resources.configs.testing import TestingConfig
from app.resources.init_app.db import db


class UnitTest(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        app = create_app()
        app.config.from_object(TestingConfig)
        app.app_context().push()
        cls.app = app.test_client()
        return super().setUpClass()

    @classmethod
    def tearDownClass(cls) -> None:
        db.session.remove()
        return super().tearDownClass()

    def setUp(self) -> None:
        db.create_all()

    def tearDown(self) -> None:
        db.drop_all()