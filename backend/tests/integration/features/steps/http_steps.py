import json

from behave import *

@given(u'the updated body request')
@given(u'the body request')
def create_body_request(context):
    context.request_body = json.loads(context.text)


@given(u'I "{http_method}" the body request to "{api_route}"')
@when(u'I "{http_method}" the body request to "{api_route}"')
def send_body_request(context, http_method, api_route):
    https_map = {
        "post": context.app.post,
        "patch": context.app.patch,
    }
    context.request_response = https_map[http_method](
        f'api/v1{api_route}',
        json=context.request_body
    )


@then(u'the response code should be "{response_code}"')
def check_response_status_code(context, response_code):
    assert context.request_response.status_code == int(response_code)

@given(u'the response body "{condition}" the "{attribute}" key')
@then(u'the response body should "{condition}" the "{attribute}" key')
def check_response_attribute(context, condition, attribute):
    if condition == 'not includes':
        assert attribute not in context.request_response.json.keys()
    else:
        assert attribute in context.request_response.json.keys()


@when(u'I send a "{http_method}" request to "{api_route}"')
@given(u'I send a "{http_method}" request to "{api_route}"')
def send_request(context, http_method, api_route):
    https_map = {
        "get": context.app.get,
        "delete": context.app.delete,
    }
    context.request_response = https_map[http_method](f'api/v1{api_route}')


@then(u'the "{key}" key in the response should have an array with length "{length}"')
def check_length_arrays_in_response(context, key, length):
    assert len(context.request_response.json[key]) == int(length)


@given(u'the response "{key}" key is "{value}"')
@then(u'the response "{key}" key should be "{value}"')
def check_response_key_value(context, key, value):
    assert context.request_response.json[key] == value


@when(u'I send a "{http_method}" request to "{api_route}"/:id')
def send_request_with_id(context, http_method, api_route):
    https_map = {
        "get": context.app.get,
        "delete": context.app.delete,
    }
    id = context.saved_values["id"]
    context.request_response = https_map[http_method](f'api/v1{api_route}/{id}')


@when(u'I "{http_method}" the body request to "{api_route}"/:id')
def step_impl(context, http_method, api_route):
    https_map = {
        "patch": context.app.patch,
    }
    id = context.saved_values["id"]
    context.request_response = https_map[http_method](
        f'api/v1{api_route}/{id}',
        json=context.request_body
    )
    print(context.request_response.json)


@given(u'I save the response "{key}"')
def save_response_key_value(context, key):
    context.saved_values = {}
    context.saved_values[key] = context.request_response.json.get("id", 0)
