import json

from behave import *
from faker import Faker

ROUTE = '/user'

@given(u'I create "{amount}" random users')
@when(u'I create "{amount}" random users')
def generating_random_users(context, amount):
    for _ in range(int(amount)):
        fake = Faker().simple_profile()
        data = {
            "username": fake["username"],
            "password": "strongpassword"
        }
        context.app.post(f'api/v1{ROUTE}', json=data)
