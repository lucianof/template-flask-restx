Feature: User

    Scenario: Creating, valid request body. POST:/user
        Given the body request
            """
            {
                "username": "johnson",
                "password": "strongpassword"
            }
            """
        When I "post" the body request to "/user"
        Then the response code should be "201"
        And the response body should "includes" the "id" key
        And the response body should "not includes" the "password" key


    Scenario: Get all, there are no users in the DB. GET:/user
        When I send a "get" request to "/user"
        Then the response code should be "200"
        And the response body should "includes" the "page" key
        And the response body should "includes" the "next_page" key
        And the response body should "includes" the "previous_page" key
        And the "data" key in the response should have an array with length "0"


    Scenario: Get all, there are users in the DB. GET:/user
        When I create "30" random users
        And I send a "get" request to "/user"
        Then the response code should be "200"
        And the response body should "includes" the "page" key
        And the response body should "includes" the "next_page" key
        And the response body should "includes" the "previous_page" key
        And the "data" key in the response should have an array with length "30"


    Scenario: Get by id, with non existing user. GET:/user/:id
        When I send a "get" request to "/user/50"
        Then the response code should be "200"
        And the response body should "includes" the "message" key


    Scenario: Get by id, with existing user. GET:/user/:id
        Given the body request
            """
            {
                "username": "johnson",
                "password": "strongpassword"
            }
            """
        And I "post" the body request to "/user"
        And I save the response "id"
        When I send a "get" request to "/user"/:id
        Then the response "username" key should be "johnson"


    Scenario: Delete existing user in db DELETE:/user/:id
        Given I create "1" random users
        And I send a "get" request to "/user/1"
        And the response body "includes" the "username" key
        When I send a "delete" request to "/user/1"
        Then the response code should be "204"
        When I send a "get" request to "/user/1"
        Then the response "message" key should be "Not found!"


    Scenario: Delete non existing user in db DELETE:/user/:id
        Given I send a "get" request to "/user/1"
        And the response "message" key is "Not found!"
        When I send a "delete" request to "/user/1"
        Then the response code should be "204"


    Scenario: Update existing user in db PATCH:/user/:id
        Given the body request
            """
            {
                "username": "johnson",
                "password": "strongpassword"
            }
            """
        And I "post" the body request to "/user"
        And I save the response "id"
        And the updated body request
            """
            {
                "username": "new_username"
            }
            """
        When I "patch" the body request to "/user"/:id
        Then the response code should be "200"
        And the response "username" key should be "new_username"


    Scenario: Update non-existing user in db PATCH:/user/:id
        Given the body request
            """
            {
                "username": "new_username"
            }
            """
        When I "patch" the body request to "/user/999"
        Then the response code should be "200"
        And the response "message" key should be "Not found!"


    Scenario: Update with invalid request PATCH:/user/:id
        Given the body request
            """
            {
                "username": "johnson",
                "password": "strongpassword"
            }
            """
        And I "post" the body request to "/user"
        And I save the response "id"
        And the updated body request
            """
            {
                "wrong_key": "new_username"
            }
            """
        When I "patch" the body request to "/user"/:id
        Then the response code should be "200"
        When I send a "get" request to "/user"/:id
        Then the response code should be "200"
        And the response body should "not includes" the "wrong_key" key
        And the response "username" key should be "johnson"
