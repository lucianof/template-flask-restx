from behave import use_fixture
from behave.model_core import Status

from tests.integration.helpers.app_instance import app_instance
from tests.integration.helpers.bot_reporter import BotReporter


def before_all(context):
    context.bot_reporter = BotReporter()
    context.app = use_fixture(app_instance, context)


def before_feature(context, feature):
    ...


def before_scenario(context, scenario):
    context.app.application.db.drop_all()
    context.app.application.db.create_all()
    ...


def after_scenario(context, scenario):
    context.app.application.db.drop_all()
    if scenario.status == Status.failed:
        context.bot_reporter.add_failed(context.feature.name, scenario.name)


def after_step(context, step):
    ...


def after_feature(context, feature):
    ...


def after_all(context):
    context.app.application.db.session.remove()
    context.bot_reporter.send_discord()
    ...
