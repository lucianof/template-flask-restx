from behave import fixture

from app import create_app
from app.resources.configs.testing import TestingConfig


@fixture
def app_instance(*args, **kwargs):
    app = create_app()
    app.config.from_object(TestingConfig)
    app.app_context().push()
    yield app.test_client()
