import os
import requests
import json


class BotReporter:
    def __init__(self):
        self.channel_id = os.environ.get('DISCORD_CHANNEL_ID', '')
        self.app_name = 'Lumar'
        self.failed_steps = []
        self.is_automated_bdd = os.environ.get('AUTOMATED_BDD', False)
        self.bot_url = os.environ.get('DISCORD_BOT_URL', '')

    @property
    def body_request(self):
        return json.dumps({
            "app_name": self.app_name,
            "failed_steps": self.failed_steps
        })

    def add_failed(self, scenario, step):
        self.failed_steps.append({"scenario": scenario, "step": step})

    def send_discord(self):
        if not self.is_automated_bdd or not self.bot_url:
            return False
        requests.post(f'{self.bot_url}/report_tests/bdd/{self.channel_id}', self.body_request)
        return True
