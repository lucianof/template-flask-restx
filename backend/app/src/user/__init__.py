from flask_restx import Namespace

api = Namespace('user', description='User related operations')
