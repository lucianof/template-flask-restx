from flask_restx import fields, reqparse

from app.resources.modules.schemas import (paginate_query_params,
                                           paginated_schema)

from .. import api

UserSchema = api.model('UserSchema', {
    'id': fields.Integer,
    'username': fields.String,
})


PaginatedUserSchema = paginated_schema(api, 'PaginatedUserSchema', UserSchema)


QueryParamsUserSchema = paginate_query_params.copy()


UserCreateParser = reqparse.RequestParser()
UserCreateParser.add_argument(
    'username',
    required=True,
    type=str,
    location='json',
)
UserCreateParser.add_argument(
    'password',
    type=str,
    location='json',
    required=True
)


UserUpdateParser = reqparse.RequestParser()
UserUpdateParser.add_argument(
    'username',
    type=str,
    location='json'
)
UserUpdateParser.add_argument(
    'password',
    type=str,
    location='json'
)
