from app.resources.modules.core import BaseCore


class UserCore(BaseCore):
    # This is where all of your app's logic goes.
    # BaseCore comes with the most common ones already implemented
    ...
