from app.resources.modules.controller import BaseController

from ..model.user_model import UserModel
from ..core.user_core import UserCore


class UserController(BaseController):
    # This will format the request from de route and format so Core can use
    ...


controller = UserController(model=UserModel, core=UserCore)
