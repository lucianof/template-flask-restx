import secrets

from sqlalchemy import Column, DateTime, Integer, String
from werkzeug.security import check_password_hash, generate_password_hash

from app.resources.init_app.db import db
from app.resources.modules.db.sqlalchemy import utcnow
from app.resources.modules.model import BaseModel


class UserModel(db.Model, BaseModel):

    __tablename__ = "user"

    id = Column(Integer, primary_key=True)

    username = Column(String(512), nullable=False, unique=True)
    created_at = Column(DateTime(timezone=True), server_default=utcnow())
    last_update = Column(DateTime(timezone=True), onupdate=utcnow())
    password_hash = Column(String(512), nullable=False)
    salt = Column(String(256), nullable=False)

    @property
    def password(self) -> None:
        raise AttributeError("Password cannot be accessed!!")

    @password.setter
    def password(self, password_to_hash) -> None:
        self.salt = secrets.token_hex(128)
        password_to_hash = password_to_hash + self.salt
        self.password_hash = generate_password_hash(password_to_hash)

    def check_password(self, password_to_compare) -> bool:
        return check_password_hash(self.password_hash, password_to_compare + self.salt)
