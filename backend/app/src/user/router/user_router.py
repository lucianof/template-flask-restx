from flask_restx import Resource

from .. import api
from ..controller.user_controller import controller
from ..schema.user_schema import (PaginatedUserSchema, QueryParamsUserSchema, UserCreateParser, UserSchema,
                                  UserUpdateParser)


@api.route('')
class UserRoute(Resource):

    @api.expect(QueryParamsUserSchema)
    @api.marshal_with(PaginatedUserSchema)
    def get(self, **kwargs):
        return controller.get_all()

    @api.expect(UserCreateParser)
    @api.marshal_with(UserSchema)
    def post(self, **kwargs):
        return controller.create()


@api.route('/<int:id>')
@api.param('id', 'The user identifier')
class UserRetrieveRoute(Resource):

    @api.marshal_with(UserSchema)
    def get(self, id: int, **kwargs):
        return controller.get_by_id(id)

    @api.expect(UserUpdateParser)
    @api.marshal_with(UserSchema)
    def patch(self, id: int, **kwargs):
        return controller.update(id)

    @api.response(204, {})
    def delete(self, id: int, **kwargs):
        return controller.delete(id)
