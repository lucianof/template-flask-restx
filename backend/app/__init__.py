from os import environ

from flask import Flask

from app.resources.configs import CONFIG
from app.resources.init_app import api, cors, migrate, db, jwt, commands


def create_app() -> Flask:
    app = Flask(__name__)

    FLASK_ENV = environ.get('FLASK_ENV')
    if FLASK_ENV not in CONFIG:
        FLASK_ENV = 'development'

    app.config.from_object(CONFIG[FLASK_ENV]())

    db.init_app(app)
    migrate.init_app(app)
    api.init_app(app)
    jwt.init_app(app)
    cors.init_app(app)
    commands.init_app(app)

    return app
