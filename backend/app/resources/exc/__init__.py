from flask_restx import fields
from sqlalchemy.exc import IntegrityError

from app.resources.init_app.api import api
from app.resources.modules.response import Response

ExceptionSchema = api.model('ErrorSchema', {
    'message': fields.String
})


class ExceptionModel(Exception):
    def __init__(self, msg='Internal error!') -> None:
        self.msg = msg
        self.__body = {'message': self.msg}
        self.response = Response(body=self.__body)


class PageNotFound(ExceptionModel):
    def __init__(self, data) -> None:
        self.msg = f'Page {data} not found!'
        super().__init__(msg=self.msg)


@api.errorhandler(PageNotFound)
def handle_page_not_found(error: ExceptionModel):
    return error.response.NOT_FOUND


class DataNotFound(ExceptionModel):
    def __init__(self) -> None:
        self.msg = 'Not found!'
        super().__init__(msg=self.msg)


@api.errorhandler(DataNotFound)
def handle_not_found(error: ExceptionModel):
    return error.response.NOT_FOUND


@api.errorhandler(IntegrityError)
def handle_integrity_error(error):
    return Response(body={'message': 'Unique constraint violated!'}).BAD_REQUEST


@api.errorhandler(ValueError)
def handle_value_error(error):
    return Response(body={'message': str(error)}).BAD_REQUEST
