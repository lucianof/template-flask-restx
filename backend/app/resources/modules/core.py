from typing import Type

from app.resources.exc import DataNotFound
from app.resources.init_app.db import db
from app.resources.modules.paginator import Paginator


class BaseCore:
    """Base class where all the logic used on controller must be, handles the requests
    to the database itself.
    Args:
            model (db.Model): _description_
    """
    def __init__(self, model: Type[db.Model], **dependencies) -> None:
        self.model = model
        self.paginator = dependencies.get("paginator_class", Paginator)

    def get_all(self, *args, **kwargs) -> db.Model:
        """Return all the values from the model table from the db, it uses the default Paginator
        class to handle the response

        Returns:
            db.Model: A object to be serialized for response.
        """
        # data_list = self.model.query.order_by(desc(self.model.id)).all()
        data_list = self.model.query.all()
        return self.paginator(data_list=data_list).data

    def get_by_id(self, id: str, *args, **kwargs) -> db.Model:
        """Return the value if the id is found on db as a HTTP response

        Args:
            id (str): the primary_key value to be found on the db.

        Returns:
            db.Model: A object to be serialized for response.
        """
        data = self.model.query.get(id)
        if not data:
            raise DataNotFound()
        return data

    def get_by(self, *args, **kwargs) -> db.Model:
        """A function to filter based on the given arguments as follows: field: Any="value"

        Returns:
            db.Model: A object to be serialized for response.
        """
        data = self.model.query.filter_by(**kwargs).first()
        if not data:
            raise DataNotFound()
        return data

    def filter_by(self, *args, **kwargs) -> db.Model:
        """A function to filter based on the given arguments as follows: field: Any="value"

        Returns:
            db.Model: A object to be serialized for response.
        """
        data_list = self.model.query.filter_by(**kwargs).all()
        return self.paginator(data_list=data_list)

    def create(self, data, *args, **kwargs) -> db.Model:
        """A function to create a object into db based data.

        Returns:
            db.Model: A object to be serialized for response.
        """
        new_data = self.model(**data)
        new_data.save()
        return new_data

    def update(self, id, data, *args, **kwargs) -> db.Model:
        """A function to update a object into db based data.

        Returns:
            db.Model: A object to be serialized for response.
        """
        actual_data = self.get_by_id(id)
        actual_data.update(data)
        return actual_data

    def delete(self, id: str) -> dict:
        """Call the delete method from the model class and handles the HTTP Response

        Args:
            id (str): the primary_key value to be found on the db.

        Returns:
            dict: An empty dict.
        """
        data = self.model.query.get(id)
        if data:
            data.delete()
        return {}
