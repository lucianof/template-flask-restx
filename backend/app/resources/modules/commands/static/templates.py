class Templates:
    def __init__(self, app_name) -> None:
        self.app_name = app_name.lower()

    @property
    def MODEL_TEMPLATE(self):
        return f"""from sqlalchemy import Column, Float, Integer, String, Boolean, DateTime

from app.resources.init_app.db import db
from app.resources.modules.db.sqlalchemy import utcnow
from app.resources.modules.model import BaseModel


class {self.app_name.title()}Model(db.Model, BaseModel):

    __tablename__ = "{self.app_name}"

    id = Column(Integer, primary_key=True)

    text_field = Column(String(512), nullable=False)
    float_field = Column(Float(), nullable=False)
    boolean_field = Column(Boolean, default=True)
    datetime_field = Column(DateTime(timezone=True))
    created_at = Column(DateTime(timezone=True), server_default=utcnow())
    last_update = Column(DateTime(timezone=True), onupdate=utcnow())
"""

    @property
    def ROUTER_TEMPLATE(self):
        return f"""from flask_restx import Resource

from .. import api
from ..controller.{self.app_name}_controller import controller
from ..schema.{self.app_name}_schema import (Paginated{self.app_name.title()}Schema, QueryParams{self.app_name.title()}Schema,
                                  {self.app_name.title()}CreateParser, {self.app_name.title()}Schema,
                                  {self.app_name.title()}UpdateParser)


@api.route('')
class {self.app_name.title()}Route(Resource):

    @api.expect(QueryParams{self.app_name.title()}Schema)
    @api.marshal_with(Paginated{self.app_name.title()}Schema)
    def get(self, **kwargs):
        return controller.get_all()

    @api.expect({self.app_name.title()}CreateParser)
    @api.marshal_with({self.app_name.title()}Schema)
    def post(self, **kwargs):
        return controller.create()


@api.route('/<int:id>')
@api.param('id', 'The {self.app_name} identifier')
class {self.app_name.title()}RetrieveRoute(Resource):

    @api.marshal_with({self.app_name.title()}Schema)
    def get(self, id: int, **kwargs):
        return controller.get_by_id(id)

    @api.expect({self.app_name.title()}UpdateParser)
    @api.marshal_with({self.app_name.title()}Schema)
    def patch(self, id: int, **kwargs):
        return controller.update(id)

    @api.response(204, {{}})
    def delete(self, id: int, **kwargs):
        return controller.delete(id)
"""

    @property
    def SCHEMA_TEMPLATE(self):
        return f"""from flask_restx import fields, reqparse

from app.resources.modules.schemas import (paginate_query_params,
                                             paginated_schema)

from .. import api

{self.app_name.title()}Schema = api.model('{self.app_name.title()}Schema', {{
    'id': fields.Integer,
    'string_field': fields.String,
    'datetime_field': fields.Datetime,
    'float_field': fields.Float
}})


Paginated{self.app_name.title()}Schema = paginated_schema(api, 'Paginated{self.app_name.title()}Schema', {self.app_name.title()}Schema)


QueryParams{self.app_name.title()}Schema = paginate_query_params.copy()


{self.app_name.title()}CreateParser = reqparse.RequestParser()
{self.app_name.title()}CreateParser.add_argument(
    'string_field',
    required=True,
    type=str,
    location='json',
)
{self.app_name.title()}CreateParser.add_argument(
    'integer_field',
    type=int,
    location='json',
    required=True
)
{self.app_name.title()}CreateParser.add_argument(
    'boolean_field',
    type=bool,
    location='json',
    required=True
)


{self.app_name.title()}UpdateParser = reqparse.RequestParser()
{self.app_name.title()}UpdateParser.add_argument(
    'string_field',
    type=str,
    location='json'
)
{self.app_name.title()}UpdateParser.add_argument(
    'integer_field',
    type=int,
    location='json'
)
{self.app_name.title()}CreateParser.add_argument(
    'boolean_field',
    type=bool,
    location='json',
)
"""

    @property
    def CORE_TEMPLATE(self):
        return f"""from app.resources.modules.core import BaseCore


class {self.app_name.title()}Core(BaseCore):
    # This is where all of your app's logic goes.
    # BaseCore comes with the most common ones already implemented
    ...
"""

    @property
    def CONTROLLER_TEMPLATE(self):
        return f"""from app.resources.modules.controller import BaseController

from ..model.{self.app_name}_model import {self.app_name.title()}Model
from ..core.{self.app_name}_core import {self.app_name.title()}Core


class {self.app_name.title()}Controller(BaseController):
    # This will format the request from de route and format so Core can use
    ...


controller = {self.app_name.title()}Controller(model={self.app_name.title()}Model, core={self.app_name.title()}Core)
"""

    @property
    def NAMESPACE(self):
        return f"""from flask_restx import Namespace

api = Namespace('{self.app_name}', description='{self.app_name.title()} related operations')
"""

    @property
    def TEST_MODEL_TEMPLATE(self):
        return f"""from tests.unit.unit_test import UnitTest
from app.src.{self.app_name}.model import {self.app_name}_model


class Test{self.app_name.title()}Model(UnitTest):
    def setUp(self) -> None:
        super().setUp()
        self.VALID_DATA = {{
            'field1': 'example1',
            'field2': 'example2'
        }}
        self.{self.app_name} = {self.app_name}_model.{self.app_name.title()}Model(**self.VALID_DATA)
        self.{self.app_name}.save()

    def test_{self.app_name}_save_into_db(self):
        new_{self.app_name} = {self.app_name}_model.{self.app_name.title()}Model(field1='example', field2='example')
        new_{self.app_name}.save()
        self.assertEqual(new_{self.app_name}.id, 2)
"""

    @property
    def TEST_CONTROLLER_TEMPLATE(self):
        return f"""# Only write tests for controller if you edited a feature or created a new one if that's not the case DELETE this file.
"""

    @property
    def TEST_CORE_TEMPLATE(self):
        return f"""# Only write tests for CORE if you edited a feature or created a new one if that's not the case DELETE this file.
"""

