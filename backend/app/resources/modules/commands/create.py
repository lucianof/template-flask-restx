import os
from pathlib import Path

from click import argument
from flask import Flask
from flask.cli import AppGroup

from .static.templates import Templates


def cli_create(app: Flask):
    cli_admin_group = AppGroup(
        'create',
        help='A group of developer usefull commands.'
    )

    @cli_admin_group.command(
        'app',
        help='Creates the directory structures of new apps.'
    )
    @argument('app_name')
    def cli_create_app(app_name: str):
        """The responsible function to create the folder structure for new apps.

        Args:
            app_name (str): The app name that will be used on the folder's name.
        """
        app_name = app_name.lower()

        DIRECTORIES = ('controller', 'core', 'model', 'router', 'schema', )
        templates = Templates(app_name)

        CURRENT_PATH = os.getcwd()
        new_path = os.path.join(CURRENT_PATH, 'app', 'src', app_name)
        os.mkdir(new_path)
        with open(os.path.join(new_path, '__init__.py'), 'w') as f:
            f.write(templates.NAMESPACE)

        for dir in DIRECTORIES:
            new_dir = os.path.join(new_path, dir)
            os.mkdir(new_dir)
            init_path = os.path.join(new_dir, '__init__.py')
            Path(init_path).touch()
            file_path = os.path.join(new_dir, f'{app_name}_{dir}.py')
            with open(file_path, 'w') as f:
                f.write(getattr(templates, f'{dir.upper()}_TEMPLATE'))

        TEST_DIRECTORIES = ('controller', 'core', 'model', )
        new_test_path = os.path.join(CURRENT_PATH, 'tests', 'unit', app_name)
        os.mkdir(new_test_path)

        for dir in TEST_DIRECTORIES:
            file_path = os.path.join(new_test_path, f'test_{app_name}_{dir}.py')
            with open(file_path, 'w') as f:
                f.write(getattr(templates, f'TEST_{dir.upper()}_TEMPLATE'))

    app.cli.add_command(cli_admin_group)
