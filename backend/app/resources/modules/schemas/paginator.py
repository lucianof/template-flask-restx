from flask_restx import fields, Namespace, reqparse


def paginated_schema(api: Namespace, name, schema):
    return api.model(name, {
        "page": fields.String,
        "previous_page": fields.String,
        "next_page": fields.String,
        "data": fields.List(fields.Nested(schema))
    })


paginate_query_params = reqparse.RequestParser()
paginate_query_params.add_argument('page', type=int, location='args', default=1)
paginate_query_params.add_argument('perPage', type=int, location='args', default=50)
