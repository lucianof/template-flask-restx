from math import ceil

from flask import request

from app.resources.exc import PageNotFound


class Paginator:
    def __init__(self, data_list: list, **kwargs) -> None:
        self.__data_list = data_list
        self.__per_page = int(request.args.get("perPage", 50))
        self.page = int(request.args.get("page", 1))
        self.__last_page = ceil(len(self.__data_list) / self.__per_page)
        self.data = self.__paginate()

    def __paginate(self) -> dict:
        if self.__last_page == 0:
            return {
                "page": self.page,
                "previous_page": None,
                "next_page": None,
                "data": []
            }

        if self.page < 1 or self.page > self.__last_page:
            raise PageNotFound(self.page)

        previous_page = None
        next_page = None

        if self.page < self.__last_page:
            next_page = self.page + 1

        if self.page > 1:
            previous_page = self.page - 1

        return {
            "page": self.page,
            "previous_page": f'page={previous_page}&perPage={self.__per_page}'
            if previous_page else previous_page,
            "next_page": f'page={next_page}&perPage={self.__per_page}' if next_page else next_page,
            "data": self.__data_list[((self.page - 1) * self.__per_page):(self.page * self.__per_page)]
        }
