from app.resources.init_app.db import db


class BaseModel:
    """A Base model with basic methods used all around
    """
    def save(self) -> None:
        """Saves the instance into the database
        """
        db.session.add(self)
        db.session.commit()

    def delete(self) -> None:
        """Delete the instance from the database, if the instance has the is_active attributes,
        instead of deleting it, it will turn into inactive
        """
        if hasattr(self, "is_active"):
            self.is_active = False
            self.save()
        else:
            db.session.delete(self)
            db.session.commit()

    def update(self, data: dict) -> None:
        """Update the instance from the values passed on the argument as a dict, doesn't create
        new attributes, only changes existing ones.

        Args:
            data (dict): the values you want to update, doesn't need to parse the values you dont
            want to change.
        """
        for key, value in data.items():
            if hasattr(self, key):
                setattr(self, key, value)
        self.save()

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__}>"
