from flask import request
from flask.wrappers import Response as HTTPResponse

from app.resources.modules.core import BaseCore
from app.resources.modules.model import BaseModel
from app.resources.modules.response import Response


class BaseController:
    """A class that implements the minimum required for a controller.

    Args:
        model (BaseModel): the class that the controller is going to work on.
        core (BaseCore): the class with all the logic that controller will call.
    """
    def __init__(self, **dependencies) -> None:
        self.model: BaseModel = dependencies.get("model", BaseModel)
        self.core: BaseCore = dependencies.get("core", BaseCore)(model=self.model)
        self.response: Response = dependencies.get("response_instance", Response)()

    def get_all(self, *args, **kwargs) -> HTTPResponse:
        self.response.body = self.core.get_all(*args, **kwargs)
        return self.response.SUCCESS

    def get_by_id(self, id, *args, **kwargs) -> HTTPResponse:
        self.response.body = self.core.get_by_id(id, *args, **kwargs)
        return self.response.SUCCESS

    def filter_by(self, *args, **kwargs) -> HTTPResponse:
        self.response.body = self.core.filter_by(*args, **kwargs)
        return self.response.SUCCESS

    def create(self, *args, **kwargs) -> HTTPResponse:
        data = kwargs.get('data', request.json)
        self.response.body = self.core.create(data=data, *args, **kwargs)
        return self.response.CREATED

    def update(self, id, *args, **kwargs) -> HTTPResponse:
        data = kwargs.get('data', request.json)
        self.response.body = self.core.update(id=id, data=data, *args, **kwargs)
        return self.response.SUCCESS

    def delete(self, id, *args, **kwargs) -> HTTPResponse:
        self.core.delete(id, *args, **kwargs)
        return self.response.NO_CONTENT
