from http import HTTPStatus

from flask import Response as HTTPResponse


class Response:
    """The response object that have the default return for http requests. Is set to mimetype
    application/json by default. You have to access the object attribute to get the response

    Args:
        body (dict): reponse's body
        headers (dict): reponse's body
    """
    def __init__(self, *args, **kwargs) -> None:
        self.body = kwargs.get("body", {})
        self.headers = kwargs.get("header", {})
        self.__status_code = HTTPStatus.NOT_FOUND

    def __response_formatter(self) -> HTTPResponse:
        """Format the responses attributes to a default return

        Returns:
            HTTPResponse: HTTP response with json body by default
        """
        return self.body, self.__status_code, self.headers

    @property
    def SUCCESS(self) -> HTTPResponse:
        """Default response for http status 200

        Returns:
            HTTPResponse: HTTP response with json body by default
        """
        self.__status_code = HTTPStatus.OK
        return self.__response_formatter()

    @property
    def CREATED(self) -> HTTPResponse:
        """Default response for http status 201

        Returns:
            HTTPResponse: HTTP response with json body by default
        """
        self.__status_code = HTTPStatus.CREATED
        return self.__response_formatter()

    @property
    def ACCEPTED(self) -> HTTPResponse:
        """Default response for http status 202

        Returns:
            HTTPResponse: HTTP response with json body by default
        """
        self.__status_code = HTTPStatus.ACCEPTED
        return self.__response_formatter()

    @property
    def NO_CONTENT(self) -> HTTPResponse:
        """Default response for http status 204

        Returns:
            HTTPResponse: HTTP response with json body by default
        """
        self.body = {}
        self.__status_code = HTTPStatus.NO_CONTENT
        return self.__response_formatter()

    @property
    def BAD_REQUEST(self) -> HTTPResponse:
        """Default response for http status 400

        Returns:
            HTTPResponse: HTTP response with json body by default
        """
        self.__status_code = HTTPStatus.BAD_REQUEST
        return self.__response_formatter()

    @property
    def UNAUTHENTICATED(self) -> HTTPResponse:
        """Default response for http status 401, if no body is provided on the instance will give a
        default message

        Returns:
            HTTPResponse: HTTP response with json body by default
        """
        self.__status_code = HTTPStatus.UNAUTHORIZED
        if not self.body:
            self.body = {
                "message": "You must be logged in to access this route"
            }
        return self.__response_formatter()

    @property
    def NON_AUTHORIZED(self) -> HTTPResponse:
        """Default response for http status 403, if no body is provided on the instance will give a
        default message

        Returns:
            HTTPResponse: HTTP response with json body by default
        """
        self.__status_code = HTTPStatus.FORBIDDEN
        if not self.body:
            self.body = {
                "message": "You are not allowed to access this route"
            }
        return self.__response_formatter()

    @property
    def NOT_FOUND(self) -> HTTPResponse:
        """Default response for http status 404, if no body is provided on the instance will give a
        default message

        Returns:
            HTTPResponse: HTTP response with json body by default
        """
        self.__status_code = HTTPStatus.OK
        if not self.body:
            self.body = {
                "message": "This method is not allowed"
            }
        return self.__response_formatter()

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__} body={str(self.body)}>"
