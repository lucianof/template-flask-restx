from os import environ

from .base import Config


class ProductionConfig(Config):

    # DB
    SQLALCHEMY_DATABASE_URI = environ.get('DATABASE_URI')

    # JWT
    JWT_SECRET_KEY = environ.get('JWT_SECRET_KEY')
