import os

from .base import Config


class TestingConfig(Config):
    TESTING = True

    CURRENT_PATH = os.getcwd()
    DB_PATH = os.path.join(CURRENT_PATH, 'test.db')
    # DB
    SQLALCHEMY_DATABASE_URI = f'sqlite:///{DB_PATH}'
