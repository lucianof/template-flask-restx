from .development import DevelopmentConfig
from .production import ProductionConfig
from .testing import TestingConfig


CONFIG = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig
}
