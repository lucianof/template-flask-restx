import os

from .base import Config


class DevelopmentConfig(Config):

    CURRENT_PATH = os.getcwd()
    DB_PATH = os.path.join(CURRENT_PATH, 'development.db')

    # DB
    SQLALCHEMY_DATABASE_URI = f'sqlite:///{DB_PATH}'
