from datetime import timedelta


class Config:
    TESTING = False

    # DB
    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/foo.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # JWT
    JWT_SECRET_KEY = 'thisisastrongJWTkey'
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(hours=1)
