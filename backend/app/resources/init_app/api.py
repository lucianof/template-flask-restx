from flask import Flask
from flask_restx import Api


api = Api(
    title='Lumar',
    version='1.0.0',
    description='This app solves some problem.',
    validate=True,
    prefix='/api/v1',
    doc='/api/v1/docs',
    default_mediatype='application/json'
)


def init_app(app: Flask) -> None:

    from app.src.user.router.user_router import api as UserApi
    api.add_namespace(UserApi, path='/user')
    
    app.api = api
    api.init_app(app)
