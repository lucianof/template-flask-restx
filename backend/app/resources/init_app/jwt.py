from environs import Env
from flask import Flask
from flask_jwt_extended import JWTManager


def init_app(app: Flask) -> None:
    JWTManager(app)

    env = Env()
    env.read_env
