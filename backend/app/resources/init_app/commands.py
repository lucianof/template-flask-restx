from flask import Flask
from app.resources.modules.commands.create import cli_create


def init_app(app: Flask) -> None:
    cli_create(app)
