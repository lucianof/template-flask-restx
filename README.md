## Repositório template para meus projetos

## BACKEND

### Testes unitários

1. Entrar na pasta do backend

   ```
   cd backend/
   ```

2. Executar os testes unitários

   ```
   coverage run
   ```

3. Avaliar cobertura de testes

   ```
   coverage report
   ```

4. Cobertura em formato html

   ```
   coverage html
   ```
